# Projectes 2020

## Escola del Treball de Barcelona

### ASIX Administració de sistemes informàtics en xarxa

#### Curs 2019-2020

#### M14 Projecte

1) Parveen Gangas: **Ldap Avançat**

https://github.com/parveen1/project-parveen


2) Jorge Pastor: **Kubernetes**

https://gitlab.com/jorgepastorr/kubernetes


3) Guillermo Ferrer: **Gabriel Messenger**

https://github.com/guibos/gabriel-messenger

https://github.com/guibos/presentation


4) Adrià Quintero: **Metasploit**

https://gitlab.com/isx47595161/m14-projecteasix


5) Miguel Amorós: **Jenkins**

https://github.com/isx46410800/M14_Jenkins


6) Pau Martín & Sergi Castellà: **Suricata**

https://gitlab.com/isx41747980/projecte-metsaploit/-/tree/master


7) Wallid el Harrak: **DevOps**

https://gitlab.com/welharrak/devops/


8) Gustavo Tello: **Selinux**

https://github.com/isx43577298/PROYECTO-2020


9) Pere Canet: **Metasploit**

https://gitlab.com/isx41747980/projecte-metsaploit/-/tree/master


10) Marc Gómez: **Kubernetes**

https://gitlab.com/marcgca/kubernetes-project


11) Sergi Iserte: **Snort**

https://gitlab.com/isx39443774/projecte-snort


12) Roberto Altamirano: **Kubernetes**

https://github.com/isx47262285/Project_kubernetes


13) Sergi Muñoz: **Grafana**

https://github.com/SergiMC/ProyectoSergiMC


 
